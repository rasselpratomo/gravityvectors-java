package com.gravity.vector;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by rasselpratomo on 4/10/16.
 */
public class Vector {
    public static void main(String[] args) {
        String shortestVector1="";
        String shortestVector2="";
        int shortestVector1idx=0;
        int shortestVector2idx=0;
        double distance;
        double shortestDistance=0.0;

        ArrayList vectorList;

        String inputFilename = "/Users/rasselpratomo/IdeaProjects/gravityvectors/sample_input_3_1000.tsv";
        String outputFilename = "/Users/rasselpratomo/IdeaProjects/gravityvectors/output.tsv";
        Vector parseCSVFile = new Vector();

        vectorList = parseCSVFile.parseTSV(inputFilename);

        for(int i=0;i<vectorList.size();i++){
            double[] arrayVectors = (double[]) vectorList.get(i);

            for(int j=0;j<vectorList.size();j++){
                double[] arrayVectors2 = (double[]) vectorList.get(j);
                double sum = 0.0;

                if (arrayVectors == arrayVectors2){
                    continue;
                }

                for(int k=0;k < arrayVectors.length;k++) {
                    sum = sum + Math.pow((arrayVectors[k] - arrayVectors2[k]), 2.0);
                }
                distance = Math.sqrt(sum);

                if (shortestDistance > distance || shortestDistance == 0.0){
                    shortestDistance = distance;
                    shortestVector1idx = vectorList.indexOf(arrayVectors)+1;
                    shortestVector2idx = vectorList.indexOf(arrayVectors2)+1;
                    shortestVector1 = Arrays.toString(arrayVectors)
                            .replace("[","").replace("]","").replace(",", "");
                    shortestVector2 = Arrays.toString(arrayVectors2)
                            .replace("[","").replace("]","").replace(",", "");
                }
            }
        }

        try {
            String[] result;
            String raw_result;

            raw_result = shortestVector1idx+":"+shortestVector1+"#"+shortestVector2idx+":"+shortestVector2;
            result = raw_result.split("#");
            CSVWriter writer = new CSVWriter(new FileWriter(outputFilename), '\t', CSVWriter.NO_QUOTE_CHARACTER);
            writer.writeNext(result);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static ArrayList parseTSV(String filename)
    {
        CSVReader reader;
        ArrayList distanceList = new ArrayList();

        try
        {
            reader = new CSVReader(new FileReader(filename), '\t');
            String[] row;

            while ((row = reader.readNext()) != null)
            {
                double[] arrayVectors = new double[row.length];

                for (int i = 0; i < row.length; i++)
                {
                    double vector;

                    vector = Double.parseDouble(row[i]);
                    arrayVectors[i] = vector;
                }
                distanceList.add(arrayVectors);
            }
        }
        catch (FileNotFoundException e)
        {
            System.err.println(e.getMessage());
        }
        catch (IOException e)
        {
            System.err.println(e.getMessage());
        }
        return distanceList;
    }

}
